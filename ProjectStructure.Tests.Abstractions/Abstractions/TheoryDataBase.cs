﻿namespace ProjectStructure.Tests.Abstractions
{
    public abstract class TheoryDataBase
    {
        public string DataDescription { get; }

        protected TheoryDataBase(string dataDescription)
        {
            DataDescription = dataDescription;
        }

        public override string ToString()
        {
            return DataDescription;
        }
    }
}
