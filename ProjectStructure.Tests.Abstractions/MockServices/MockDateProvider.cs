﻿using System;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Tests.Abstractions.MockServices
{
    public class MockDateProvider : IDateProvider
    {
        public DateTimeOffset Now { get; }

        public MockDateProvider(DateTimeOffset now)
        {
            Now = now;
        }
    }
}
