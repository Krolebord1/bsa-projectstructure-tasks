﻿using System.Collections.Generic;
using Microsoft.Extensions.Options;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.Data.Seed.Options;

namespace ProjectStructure.Data.Seed.Services
{
    public class SeedingProvider : ISeedingProvider
    {
        private readonly IDataLoader _loader;

        private readonly SeedOptions _options;

        private List<UserReadDTO>? _users;
        private List<ProjectReadDTO>? _projects;
        private List<TeamReadDTO>? _teams;
        private List<TaskReadDTO>? _tasks;

        public SeedingProvider(IDataLoader loader, IOptions<SeedOptions> options)
        {
            _loader = loader;
            _options = options.Value;
        }

        public IList<UserReadDTO> GetUsers()
        {
            if (_users == null)
                LoadData();

            return _users!;
        }

        public IList<ProjectReadDTO> GetProjects()
        {
            if (_projects == null)
                LoadData();

            return _projects!;
        }

        public IList<TeamReadDTO> GetTeams()
        {
            if (_teams == null)
                LoadData();

            return _teams!;
        }

        public IList<TaskReadDTO> GetTasks()
        {
            if (_tasks == null)
                LoadData();

            return _tasks!;
        }

        private void LoadData()
        {
            _users = _loader.GetDeserialized<List<UserReadDTO>>(_options.UsersPath);
            _projects = _loader.GetDeserialized<List<ProjectReadDTO>>(_options.ProjectsPath);
            _teams = _loader.GetDeserialized<List<TeamReadDTO>>(_options.TeamsPath);
            _tasks = _loader.GetDeserialized<List<TaskReadDTO>>(_options.TasksPath);
        }
    }
}
