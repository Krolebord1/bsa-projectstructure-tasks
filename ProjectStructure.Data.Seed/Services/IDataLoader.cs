﻿namespace ProjectStructure.Data.Seed.Services
{
    public interface IDataLoader
    {
        public T GetDeserialized<T>(string path);
    }
}
