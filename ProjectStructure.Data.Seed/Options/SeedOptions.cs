﻿namespace ProjectStructure.Data.Seed.Options
{
    public class SeedOptions
    {
        public const string Key = "SeedOptions";

        public string UsersPath { get; set; } = string.Empty;

        public string ProjectsPath { get; set; } = string.Empty;

        public string TeamsPath { get; set; } = string.Empty;

        public string TasksPath { get; set; } = string.Empty;
    }
}
