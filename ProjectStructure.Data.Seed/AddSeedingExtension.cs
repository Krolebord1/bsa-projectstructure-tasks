﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.Data.Seed.Services;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.Seed
{
    public static class AddSeedingExtension
    {
        public static void Seed(this ModelBuilder builder, ISeedingProvider seedingProvider)
        {
            builder.Entity<User>().HasData(seedingProvider.GetUsers());
            builder.Entity<Project>().HasData(seedingProvider.GetProjects());
            builder.Entity<Team>().HasData(seedingProvider.GetTeams());
            builder.Entity<UserTask>().HasData(seedingProvider.GetTasks());
        }
    }
}
