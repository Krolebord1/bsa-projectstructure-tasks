﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProjectStructure.Data.Seed;
using ProjectStructure.Data.Seed.Services;

namespace ProjectStructure.Data
{
    public class AppContext : DbContext
    {
        private readonly ISeedingProvider _seedingProvider;

        public AppContext(DbContextOptions options, IConfiguration configuration, ISeedingProvider seeding) : base(options)
        {
            _seedingProvider = seeding;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppContext).Assembly);

            modelBuilder.Seed(_seedingProvider);

            base.OnModelCreating(modelBuilder);
        }
    }
}
