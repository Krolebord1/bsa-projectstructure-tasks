﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data.Repositories
{
    public class ReadOnlyRepository<TEntity> : IReadRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly DbSet<TEntity> entities;

        public ReadOnlyRepository(AppContext context)
        {
            entities = context.Set<TEntity>();
        }

        public IQueryable<TEntity> ReadQuery() =>
            entities.AsNoTrackingWithIdentityResolution().AsQueryable();

        public async Task<TEntity?> ReadAsync(int id)
        {
            return await ReadQuery().FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public async Task<IList<TEntity>> ReadAllAsync()
        {
            return await ReadQuery().ToListAsync();
        }
    }
}
