﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.EntityConfigurationExtensions
{
    public static class NamePropertyExtension
    {
        public static PropertyBuilder<string> IsName(this PropertyBuilder<string> property) =>
            property
                .IsRequired()
                .HasMaxLength(EntityConstants.MaxNameLength);
    }
}
