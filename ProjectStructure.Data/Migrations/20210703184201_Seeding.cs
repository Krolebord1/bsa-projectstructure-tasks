﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
#pragma warning disable 8625

namespace ProjectStructure.Data.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 1, new DateTimeOffset(new DateTime(2017, 3, 31, 2, 29, 28, 374, DateTimeKind.Unspecified).AddTicks(504), new TimeSpan(0, 0, 0, 0, 0)), "Durgan Group" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[] { 2, new DateTimeOffset(new DateTime(2019, 2, 21, 15, 47, 30, 379, DateTimeKind.Unspecified).AddTicks(7852), new TimeSpan(0, 0, 0, 0, 0)), "Kassulke LLC" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTimeOffset(new DateTime(1987, 3, 12, 9, 11, 1, 353, DateTimeKind.Unspecified).AddTicks(6142), new TimeSpan(0, 0, 0, 0, 0)), "Anne.Collier@hotmail.com", "Anne", "Collier", new DateTimeOffset(new DateTime(2020, 4, 24, 3, 13, 17, 101, DateTimeKind.Unspecified).AddTicks(2910), new TimeSpan(0, 0, 0, 0, 0)), 1 },
                    { 2, new DateTimeOffset(new DateTime(2005, 3, 7, 1, 42, 11, 466, DateTimeKind.Unspecified).AddTicks(9734), new TimeSpan(0, 0, 0, 0, 0)), "Erin.Pacocha14@hotmail.com", "Erin", "Pacocha", new DateTimeOffset(new DateTime(2020, 8, 14, 6, 1, 52, 692, DateTimeKind.Unspecified).AddTicks(5066), new TimeSpan(0, 0, 0, 0, 0)), 1 },
                    { 3, new DateTimeOffset(new DateTime(1963, 1, 27, 12, 3, 1, 83, DateTimeKind.Unspecified).AddTicks(1205), new TimeSpan(0, 0, 0, 0, 0)), "Jennifer37@hotmail.com", "Jennifer", "Haag", new DateTimeOffset(new DateTime(2019, 11, 14, 18, 1, 12, 835, DateTimeKind.Unspecified).AddTicks(1980), new TimeSpan(0, 0, 0, 0, 0)), 1 },
                    { 4, new DateTimeOffset(new DateTime(1965, 5, 26, 18, 27, 15, 225, DateTimeKind.Unspecified).AddTicks(5076), new TimeSpan(0, 0, 0, 0, 0)), "Elizabeth94@hotmail.com", "Elizabeth", "Koepp", new DateTimeOffset(new DateTime(2021, 6, 11, 19, 5, 36, 352, DateTimeKind.Unspecified).AddTicks(3570), new TimeSpan(0, 0, 0, 0, 0)), 2 },
                    { 5, new DateTimeOffset(new DateTime(2010, 12, 5, 21, 48, 7, 284, DateTimeKind.Unspecified).AddTicks(6901), new TimeSpan(0, 0, 0, 0, 0)), "Sadie15@yahoo.com", "Sadie", "Bernhard", new DateTimeOffset(new DateTime(2021, 4, 15, 21, 8, 32, 623, DateTimeKind.Unspecified).AddTicks(9838), new TimeSpan(0, 0, 0, 0, 0)), 2 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTimeOffset(new DateTime(2020, 1, 16, 7, 5, 5, 697, DateTimeKind.Unspecified).AddTicks(7700), new TimeSpan(0, 0, 0, 0, 0)), new DateTimeOffset(new DateTime(2021, 10, 3, 14, 57, 28, 977, DateTimeKind.Unspecified).AddTicks(4796), new TimeSpan(0, 0, 0, 0, 0)), "Officiis maiores exercitationem.", "THX", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 1, new DateTimeOffset(new DateTime(2020, 6, 9, 17, 10, 47, 907, DateTimeKind.Unspecified).AddTicks(7465), new TimeSpan(0, 0, 0, 0, 0)), new DateTimeOffset(new DateTime(2021, 11, 22, 0, 45, 47, 205, DateTimeKind.Unspecified).AddTicks(6714), new TimeSpan(0, 0, 0, 0, 0)), "Praesentium est similique velit libero inventore totam.", "Cambridgeshire", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, 2, new DateTimeOffset(new DateTime(2020, 3, 29, 20, 50, 9, 46, DateTimeKind.Unspecified).AddTicks(8406), new TimeSpan(0, 0, 0, 0, 0)), new DateTimeOffset(new DateTime(2021, 8, 2, 2, 31, 39, 328, DateTimeKind.Unspecified).AddTicks(3064), new TimeSpan(0, 0, 0, 0, 0)), "Est doloribus expedita vel distinctio.", "protocol", 2 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 1, new DateTimeOffset(new DateTime(2020, 5, 15, 22, 50, 46, 86, DateTimeKind.Unspecified).AddTicks(832), new TimeSpan(0, 0, 0, 0, 0)), "Quo sint aut et ea voluptatem omnis ut.", null, "real-time", 1, 1, (byte)2 },
                    { 2, new DateTimeOffset(new DateTime(2019, 2, 15, 15, 6, 52, 60, DateTimeKind.Unspecified).AddTicks(666), new TimeSpan(0, 0, 0, 0, 0)), "Eum a eum.", null, "product Direct", 2, 1, (byte)2 },
                    { 3, new DateTimeOffset(new DateTime(2017, 8, 16, 6, 13, 44, 577, DateTimeKind.Unspecified).AddTicks(3845), new TimeSpan(0, 0, 0, 0, 0)), "Sint voluptatem quas.", new DateTimeOffset(new DateTime(2020, 9, 9, 6, 34, 47, 460, DateTimeKind.Unspecified).AddTicks(2160), new TimeSpan(0, 0, 0, 0, 0)), "bypass", 4, 2, (byte)2 },
                    { 4, new DateTimeOffset(new DateTime(2018, 10, 19, 0, 58, 34, 804, DateTimeKind.Unspecified).AddTicks(5103), new TimeSpan(0, 0, 0, 0, 0)), "Delectus quibusdam id quia iure neque maiores molestias sed aut.", null, "withdrawal contextually", 4, 2, (byte)2 },
                    { 5, new DateTimeOffset(new DateTime(2018, 6, 15, 6, 3, 48, 73, DateTimeKind.Unspecified).AddTicks(2466), new TimeSpan(0, 0, 0, 0, 0)), "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", null, "mobile Organized", 5, 2, (byte)2 },
                    { 6, new DateTimeOffset(new DateTime(2020, 5, 21, 14, 56, 53, 811, DateTimeKind.Unspecified).AddTicks(7818), new TimeSpan(0, 0, 0, 0, 0)), "Reiciendis iusto rerum non et aut eaque.", null, "world-class Circles", 5, 2, (byte)2 },
                    { 7, new DateTimeOffset(new DateTime(2018, 7, 10, 16, 21, 12, 88, DateTimeKind.Unspecified).AddTicks(6153), new TimeSpan(0, 0, 0, 0, 0)), "Et rerum ad.", null, "Automotive & Tools", 5, 2, (byte)2 },
                    { 8, new DateTimeOffset(new DateTime(2019, 5, 7, 20, 29, 10, 58, DateTimeKind.Unspecified).AddTicks(2950), new TimeSpan(0, 0, 0, 0, 0)), "Voluptas nostrum sint.", null, "payment methodologies", 4, 3, (byte)2 },
                    { 9, new DateTimeOffset(new DateTime(2020, 6, 19, 11, 42, 55, 73, DateTimeKind.Unspecified).AddTicks(8847), new TimeSpan(0, 0, 0, 0, 0)), "Rerum iure soluta consequatur velit aut.", null, "Borders Mountain", 4, 3, (byte)2 },
                    { 10, new DateTimeOffset(new DateTime(2020, 4, 14, 12, 55, 4, 403, DateTimeKind.Unspecified).AddTicks(1688), new TimeSpan(0, 0, 0, 0, 0)), "Iure sequi unde.", null, "navigate", 5, 3, (byte)2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
