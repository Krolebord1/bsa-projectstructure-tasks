﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Data.Interfaces
{
    public interface IDbContext
    {
        public IList<TEntity> GetEntities<TEntity>() where TEntity : class, IEntity;

        public Task<int> SaveChangesAsync();
    }
}
