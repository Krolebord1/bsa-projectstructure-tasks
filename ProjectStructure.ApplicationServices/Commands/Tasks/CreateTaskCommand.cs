﻿using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.Commands.Tasks
{
    public class CreateTaskCommand : IRequest
    {
        public TaskWriteDTO TaskDTO { get; }

        public CreateTaskCommand(TaskWriteDTO taskDTO)
        {
            TaskDTO = taskDTO;
        }
    }
}
