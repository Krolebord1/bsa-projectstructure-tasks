﻿using System.Collections.Generic;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetUserUnfinishedTasksQuery : IRequest<OneOf<IEnumerable<TaskReadDTO>, NotFound>>
    {
        public int UserId { get; }

        public GetUserUnfinishedTasksQuery(int userId)
        {
            UserId = userId;
        }
    }
}
