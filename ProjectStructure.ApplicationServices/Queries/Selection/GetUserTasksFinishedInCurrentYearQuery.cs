﻿using System.Collections.Generic;
using MediatR;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetUserTasksFinishedInCurrentYearQuery : IRequest<IEnumerable<(int, string)>>
    {
        public int UserId { get; }

        public GetUserTasksFinishedInCurrentYearQuery(int userId)
       {
           UserId = userId;
       }
    }
}
