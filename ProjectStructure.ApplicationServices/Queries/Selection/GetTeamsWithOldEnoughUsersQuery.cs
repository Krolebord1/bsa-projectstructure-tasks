﻿using System.Collections.Generic;
using MediatR;
using ProjectStructure.ApplicationServices.DTOs;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetTeamsWithOldEnoughUsersQuery : IRequest<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>>
    {
        public int OldEnoughAge => 10;
    }
}
