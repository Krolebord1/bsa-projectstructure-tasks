﻿using System.Collections.Generic;
using MediatR;

namespace ProjectStructure.ApplicationServices.Queries
{
    public class GetEntitiesQuery<TReadDTO> : IRequest<IEnumerable<TReadDTO>> { }
}
