﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Projects;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Projects
{
    public class UpdateProjectHandler : IRequestHandler<UpdateProjectCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<Project> _projectsRepository;
        private readonly IMapper _mapper;

        public UpdateProjectHandler(IRepository<Project> projectsRepository, IMapper mapper)
        {
            _projectsRepository = projectsRepository;
            _mapper = mapper;
        }

        public async Task<OneOf<Success, NotFound>> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await _projectsRepository.ReadAsync(request.Id);

            if (project == null)
                return new NotFound();

            _mapper.Map(request.ProjectDTO, project);

            await _projectsRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
