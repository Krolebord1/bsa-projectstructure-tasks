﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Projects
{
    public class GetProjectByIdHandler : GetEntityByIdHandler<Project, ProjectReadDTO>
    {
        public GetProjectByIdHandler(IReadRepository<Project> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
