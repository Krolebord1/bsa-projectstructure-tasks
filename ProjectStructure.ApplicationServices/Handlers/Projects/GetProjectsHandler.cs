﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Projects
{
    public class GetProjectsHandler : GetEntitiesHandler<Project, ProjectReadDTO>
    {
        public GetProjectsHandler(IReadRepository<Project> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
