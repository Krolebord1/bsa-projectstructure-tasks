﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Tasks
{
    public class GetTasksHandler : GetEntitiesHandler<UserTask, TaskReadDTO>
    {
        public GetTasksHandler(IReadRepository<UserTask> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
