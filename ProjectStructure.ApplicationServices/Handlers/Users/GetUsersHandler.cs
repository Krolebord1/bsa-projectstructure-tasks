﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Handlers.Common;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Users
{
    public class GetUsersHandler : GetEntitiesHandler<User, UserReadDTO>
    {
        public GetUsersHandler(IReadRepository<User> repository, IMapper mapper) : base(repository, mapper) { }
    }
}
