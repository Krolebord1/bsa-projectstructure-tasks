﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetUserTasksHandler : IRequestHandler<GetUserTasksQuery, IEnumerable<TaskReadDTO>>
    {
        private IReadRepository<UserTask> _tasksRepository;
        private IMapper _mapper;

        public GetUserTasksHandler(IReadRepository<UserTask> tasksRepository, IMapper mapper)
        {
            _tasksRepository = tasksRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TaskReadDTO>> Handle(GetUserTasksQuery request, CancellationToken cancellationToken)
        {
            var tasks = await _tasksRepository.ReadQuery()
                .Where(x => x.PerformerId == request.UserId)
                .Where(x => x.Name.Length < request.MaxTaskNameLength)
                .ToListAsync();

            return tasks.Select(task => _mapper.Map<UserTask, TaskReadDTO>(task));
        }
    }
}
