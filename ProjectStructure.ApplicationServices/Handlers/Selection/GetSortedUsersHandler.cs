﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetSortedUsersHandler : IRequestHandler<GetSortedUsersQuery, IEnumerable<UserReadDTO>>
    {
        private readonly IReadRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public GetSortedUsersHandler(IReadRepository<User> usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserReadDTO>> Handle(GetSortedUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await _usersRepository.ReadAllAsync();

            return users
                .OrderBy(user => user.FirstName)
                .Select(user => _mapper.Map<User, UserReadDTO>(user));
        }
    }
}
