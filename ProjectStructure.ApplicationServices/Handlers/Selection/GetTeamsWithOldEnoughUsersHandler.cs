﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetTeamsWithOldEnoughUsersHandler : IRequestHandler<GetTeamsWithOldEnoughUsersQuery, IEnumerable<(int, string, IEnumerable<UserReadDTO>)>>
    {
        private readonly IReadRepository<Team> _teamsRepository;
        private readonly IMapper _mapper;
        private readonly IDateProvider _dateProvider;

        public GetTeamsWithOldEnoughUsersHandler(IReadRepository<Team> teamsRepository, IDateProvider dateProvider, IMapper mapper)
        {
            _teamsRepository = teamsRepository;
            _dateProvider = dateProvider;
            _mapper = mapper;
        }

        public async Task<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>> Handle(GetTeamsWithOldEnoughUsersQuery request, CancellationToken cancellationToken)
        {
            int maxBirthYear = _dateProvider.Now.Year - request.OldEnoughAge - 1;

            var teams = await _teamsRepository.ReadQuery()
                .Include(x => x.Users)
                .ToListAsync();

            return teams
                .Select(
                    team => (
                        team.Id,
                        team.Name,
                        team.Users.Where(user => user.BirthDay.Year < maxBirthYear).Select(user => _mapper.Map<User, UserReadDTO>(user))
                    )
                );
        }
    }
}
