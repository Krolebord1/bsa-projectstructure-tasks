﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetUserTasksFinishedInCurrentYearHandler : IRequestHandler<GetUserTasksFinishedInCurrentYearQuery, IEnumerable<(int, string)>>
    {
        private IReadRepository<UserTask> _tasksRepository;
        private IDateProvider _dateProvider;

        public GetUserTasksFinishedInCurrentYearHandler(IReadRepository<UserTask> tasksRepository, IDateProvider dateProvider)
        {
            _tasksRepository = tasksRepository;
            _dateProvider = dateProvider;
        }

        public async Task<IEnumerable<(int, string)>> Handle(GetUserTasksFinishedInCurrentYearQuery request, CancellationToken cancellationToken)
        {
            var currentYear = _dateProvider.Now.Year;

            var tasks = await _tasksRepository.ReadQuery()
                .Where(x => x.PerformerId == request.UserId)
                .Where(x => x.FinishedAt != null && x.FinishedAt.Value.Year == currentYear)
                .ToListAsync();

            return tasks
                .Select(task => (task.Id, task.Name)).ToList();
        }
    }
}
