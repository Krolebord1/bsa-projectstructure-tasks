﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.Commands.Teams;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Teams
{
    public class CreateTeamHandler : IRequestHandler<CreateTeamCommand>
    {
        private readonly IRepository<Team> _teamsRepository;
        private readonly IMapper _mapper;

        public CreateTeamHandler(IRepository<Team> teamsRepository, IMapper mapper)
        {
            _teamsRepository = teamsRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var team = _mapper.Map<TeamWriteDTO, Team>(request.TeamDTO);

            _teamsRepository.Add(team);

            await _teamsRepository.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
