﻿using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.DTOs
{
    public record UserSummaryDTO(
        UserReadDTO User,
        ProjectReadDTO? LastProject,
        int LastProjectTaskCount,
        int UnfinishedTasksCount,
        TaskReadDTO? LongestFinishedTask
    )
    {
        public override string ToString() =>
            $"User: {User.FirstName} {User.LastName}" +
            $"\n\tLast project: {LastProject?.Name}" +
            $"\n\tLast project tasks count: {LastProjectTaskCount.ToString()}" +
            $"\n\tUnfinished tasks count: {UnfinishedTasksCount.ToString()}" +
            $"\n\tLongest unfinished task: {LongestFinishedTask?.Name}";
    }
}
