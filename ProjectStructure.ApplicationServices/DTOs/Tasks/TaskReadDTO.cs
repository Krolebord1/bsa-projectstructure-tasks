﻿using System;
using ProjectStructure.Domain.Enums;

namespace ProjectStructure.ApplicationServices.DTOs.Tasks
{
    public record TaskReadDTO(
        int Id,
        int ProjectId,
        int? PerformerId,
        string Name,
        string Description,
        TaskState State,
        DateTimeOffset CreatedAt,
        DateTimeOffset? FinishedAt
    );
}
