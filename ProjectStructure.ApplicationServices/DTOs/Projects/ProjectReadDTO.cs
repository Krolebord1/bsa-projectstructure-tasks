﻿using System;

namespace ProjectStructure.ApplicationServices.DTOs.Projects
{
    public record ProjectReadDTO(
        int Id,
        int? AuthorId,
        int? TeamId,
        string Name,
        string Description,
        DateTimeOffset Deadline,
        DateTimeOffset CreatedAt
    );
}
