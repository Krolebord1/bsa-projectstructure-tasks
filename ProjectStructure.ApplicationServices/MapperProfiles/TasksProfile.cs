﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ApplicationServices.MapperProfiles
{
    public class TasksProfile : Profile
    {
        public TasksProfile()
        {
            CreateMap<UserTask, TaskReadDTO>().ReverseMap();
            CreateMap<TaskWriteDTO, UserTask>();
        }
    }
}
