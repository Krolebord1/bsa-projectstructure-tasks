﻿using System;
using System.Collections.Generic;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Domain.Entities
{
    public record User : IEntity
    {
        public int Id { get; set; }

        public string FirstName { get; set; } = string.Empty;

        public string LastName { get; set; } = string.Empty;

        public string Email { get; set; } = string.Empty;

        public DateTimeOffset RegisteredAt { get; set; }

        public DateTimeOffset BirthDay { get; set; }

        public int? TeamId { get; set; }
        public Team? Team { get; set; }

        public IList<UserTask> Tasks { get; set; } = new List<UserTask>();

        public string FullName => $"{FirstName} {LastName}";
    }
}
