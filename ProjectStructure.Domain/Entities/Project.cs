﻿using System;
using System.Collections.Generic;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Domain.Entities
{
    public class Project : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public DateTimeOffset Deadline { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public int? AuthorId { get; set; }
        public User? Author { get; set; }

        public int? TeamId { get; set; }
        public Team? Team { get; set; }

        public IList<UserTask> Tasks { get; set; } = new List<UserTask>();
    }
}
