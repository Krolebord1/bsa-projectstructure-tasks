﻿using System;
using System.Collections.Generic;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Domain.Entities
{
    public record Team : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public DateTimeOffset CreatedAt { get; set; }

        public IList<Project> Projects { get; set; } = new List<Project>();

        public IList<User> Users { get; set; } = new List<User>();
    }
}
