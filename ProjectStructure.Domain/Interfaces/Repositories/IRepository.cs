﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntity> : IReadRepository<TEntity>, IWriteRepository<TEntity>
        where TEntity : class, IEntity
    {
        public Task<TEntity?> GetAsync(int id);

        public Task<IList<TEntity>> GetAllAsync();
    }
}
