﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Domain.Interfaces.Repositories
{
    public interface IReadRepository<TEntity> where TEntity : class, IEntity
    {
        public IQueryable<TEntity> ReadQuery();

        public Task<TEntity?> ReadAsync(int id);

        public Task<IList<TEntity>> ReadAllAsync();
    }
}
