﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.ApplicationServices.Commands.Projects;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Queries;

namespace ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route(APIRoutes.ProjectsController)]
    public class ProjectsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProjectsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectReadDTO>>> GetProjects()
        {
            var request = new GetEntitiesQuery<ProjectReadDTO>();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<ProjectReadDTO>> GetProjectById([FromRoute] int id)
        {
            var request = new GetEntityByIdQuery<ProjectReadDTO>(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult<ProjectReadDTO>>(
                project => Ok(project),
                notFound => NotFound()
            );
        }

        [HttpPost]
        public async Task<ActionResult> CreateProject([FromBody] ProjectWriteDTO projectDto)
        {
            var request = new CreateProjectCommand(projectDto);
            await _mediator.Send(request);

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateProject([FromRoute] int id, [FromBody] ProjectWriteDTO projectDto)
        {
            var request = new UpdateProjectCommand(id, projectDto);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => NoContent(),
                notFound => NotFound()
            );
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteProject([FromRoute] int id)
        {
            var request = new DeleteProjectCommand(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => Ok(),
                notFound => NotFound()
            );
        }
    }
}
