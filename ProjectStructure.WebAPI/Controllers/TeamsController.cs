﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.ApplicationServices.Commands.Teams;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.ApplicationServices.Queries;

namespace ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route(APIRoutes.TeamsController)]
    public class TeamsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TeamsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamReadDTO>>> GetTeams()
        {
            var request = new GetEntitiesQuery<TeamReadDTO>();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TeamReadDTO>> GetTeamById([FromRoute] int id)
        {
            var request = new GetEntityByIdQuery<TeamReadDTO>(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult<TeamReadDTO>>(
                team => Ok(team),
                notFound => NotFound()
            );
        }

        [HttpPost]
        public async Task<ActionResult> CreateTeam([FromBody] TeamWriteDTO teamDto)
        {
            var request = new CreateTeamCommand(teamDto);
            await _mediator.Send(request);

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateTeam([FromRoute] int id, [FromBody] TeamWriteDTO teamDto)
        {
            var request = new UpdateTeamCommand(id, teamDto);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => NoContent(),
                notFound => NotFound()
            );
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteTeam([FromRoute] int id)
        {
            var request = new DeleteTeamCommand(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => Ok(),
                notFound => NotFound()
            );
        }
    }
}
