﻿namespace ProjectStructure.WebAPI
{
    public class APIRoutes
    {
        public const string UsersController = "api/users";

        public const string ProjectsController = "api/projects";

        public const string TeamsController = "api/teams";

        public const string TasksController = "api/tasks";

        public const string SelectionsController = "api/selections";
    }
}
