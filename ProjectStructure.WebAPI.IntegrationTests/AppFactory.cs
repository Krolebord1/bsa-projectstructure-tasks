﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using ProjectStructure.WebAPI;
using AppContext = ProjectStructure.Data.AppContext;

namespace ProjectStructure.Tests.Integration
{
    public class AppFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.RemoveAll(typeof(DbContextOptions<AppContext>));
                services.RemoveAll(typeof(AppContext));

                services.AddDbContext<AppContext>(options =>
                {
                    options.UseInMemoryDatabase("TestDatabase");
                });

                var provider = services.BuildServiceProvider();

                using var scope = provider.CreateScope();

                var scopedProvider = scope.ServiceProvider;
                var db = scopedProvider.GetRequiredService<AppContext>();

                db.Database.EnsureCreated();
            });
        }
    }
}
