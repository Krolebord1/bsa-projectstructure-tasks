﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using Xunit;


namespace ProjectStructure.Tests.Integration.Controllers
{
    public class ProjectsControllerTest : IClassFixture<AppFactory>
    {
        private readonly HttpClient _client;

        public ProjectsControllerTest(AppFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task AddValidProject_ThenResponseWithCode201()
        {
            var projectDto = new ProjectWriteDTO(
                null,
                null,
                "Best project eva",
                "Description is not important",
                new DateTimeOffset().AddYears(5000)
            );

            var json = JsonConvert.SerializeObject(projectDto);

            var response = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task AddInvalidProject_ThenResponseWithCode400()
        {
            var projectDto = new {
                ProjectName = "Best project eva but super long name can cause some trouble",
                SomeDescription = "Description is not important",
                Something = new DateTimeOffset().AddYears(5000)
            };

            var json = JsonConvert.SerializeObject(projectDto);

            var response = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
