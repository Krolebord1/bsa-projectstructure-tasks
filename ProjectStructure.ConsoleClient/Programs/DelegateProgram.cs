﻿using System;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Programs
{
    public class DelegateProgram : AsyncProgramBase
    {
        private Func<Task> _delegate;

        public DelegateProgram(Func<Task> func)
        {
            _delegate = func;
        }

        public override Task RunAsync()
        {
            return _delegate();
        }
    }
}
