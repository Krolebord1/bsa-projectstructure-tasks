﻿using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Interfaces
{
    public interface IAsyncProgram
    {
        public Task RunAsync();
    }
}
