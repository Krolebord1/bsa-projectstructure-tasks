﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ProjectStructure.ConsoleClient.Services
{
    public class DataSender : IDataSender
    {
        private readonly HttpClient _client;

        public DataSender()
        {
            _client = new HttpClient();
        }

        public async Task<HttpStatusCode> PostAsync<T>(string uri, T data)
        {
            var json = JsonConvert.SerializeObject(data);
            var response = await _client.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return response.StatusCode;
        }

        public async Task<HttpStatusCode> PutAsync<T>(string uri, T data)
        {
            var json = JsonConvert.SerializeObject(data);
            var response = await _client.PutAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return response.StatusCode;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
