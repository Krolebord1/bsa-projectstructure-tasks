﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public interface IDataSender : IDisposable
    {
        public Task<HttpStatusCode> PostAsync<T>(string uri, T data);

        public Task<HttpStatusCode> PutAsync<T>(string uri, T data);
    }
}
