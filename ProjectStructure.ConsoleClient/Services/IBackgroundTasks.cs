﻿using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public interface IBackgroundTasks
    {
        public Task StartAsync();

        public Task StopAsync();
    }
}
