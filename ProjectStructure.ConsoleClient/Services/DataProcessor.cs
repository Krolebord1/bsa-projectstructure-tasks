﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Timers;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public class DataProcessor : IDataProcessor
    {
        private readonly IDataLoader _loader;
        private readonly IDataSender _sender;

        public DataProcessor(IDataLoader loader, IDataSender sender)
        {
            _loader = loader;
            _sender = sender;
        }

        public void Dispose()
        {
            _loader.Dispose();
        }

        public async Task<IDictionary<ProjectReadDTO, int>> GetUserTasksCount(int userId)
        {
            var result =
                await _loader.GetDeserializedAsync<IEnumerable<KeyValuePair<ProjectReadDTO, int>>>(ApiUrls.Selections.GetUserTasksCount(userId))
                ?? new Dictionary<ProjectReadDTO, int>();

            return result.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public async Task<IEnumerable<TaskReadDTO>> GetUserTasks(int userId)
        {
            return await _loader.GetDeserializedAsync<IEnumerable<TaskReadDTO>>(ApiUrls.Selections.GetUserTasks(userId))
                   ?? new List<TaskReadDTO>();
        }

        public async Task<IEnumerable<(int, string)>> GetUserTasksFinishedInCurrentYear(int userId)
        {
            return await _loader.GetDeserializedAsync<IEnumerable<(int, string)>>(ApiUrls.Selections.GetUserTasksFinishedInCurrentYear(userId))
                   ?? new List<(int, string)>();
        }

        public async Task<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>> GetTeamsWithOldEnoughUsers()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<(int, string, IEnumerable<UserReadDTO>)>>(ApiUrls.Selections.GetTeamsWithOldEnoughUsers())
                   ?? new List<(int, string, IEnumerable<UserReadDTO>)>();
        }

        public async Task<IEnumerable<UserReadDTO>> GetSortedUsers()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<UserReadDTO>>(ApiUrls.Selections.GetSortedUsers())
                ?? new List<UserReadDTO>();
        }

        public async Task<UserSummaryDTO?> GetUserSummary(int userId)
        {
            return await _loader.GetDeserializedAsync<UserSummaryDTO>(ApiUrls.Selections.GetUserSummary(userId));
        }

        public async Task<IEnumerable<ProjectSummaryDTO>> GetProjectSummary()
        {
            return await _loader.GetDeserializedAsync<IEnumerable<ProjectSummaryDTO>>(ApiUrls.Selections.GetProjectSummary())
                   ?? new List<ProjectSummaryDTO>();
        }

        public Task<int> MarkRandomTaskWithDelay(int milliseconds)
        {
            var tcs = new TaskCompletionSource<int>();

            var timer = new Timer
            {
                Interval = milliseconds,
                AutoReset = false,
                Enabled = true
            };

            timer.Elapsed += async (sender, args) =>
            {
                timer.Dispose();

                try
                {
                    var tasks = await _loader.GetDeserializedAsync<List<TaskReadDTO>>(ApiUrls.Tasks.GetAll());

                    if(tasks == null || !tasks.Any())
                        return;

                    var selectedTask = tasks[new Random().Next(0, tasks.Count)];

                    var taskWriteDto = new TaskWriteDTO(
                        Name: selectedTask.Name,
                        Description: selectedTask.Description,
                        State: selectedTask.State,
                        FinishedAt: DateTimeOffset.Now,
                        ProjectId: selectedTask.ProjectId,
                        PerformerId: selectedTask.PerformerId
                    );

                    var statusCode = await _sender.PutAsync(ApiUrls.Tasks.PutUpdate(selectedTask.Id), taskWriteDto);

                    if (statusCode == HttpStatusCode.NoContent)
                        tcs.SetResult(selectedTask.Id);
                    else
                        tcs.SetException(new DataLoadingException($"Couldn't mark task id={selectedTask.Id}. Server responded with {statusCode.ToString()}"));
                }
                catch(Exception e)
                {
                    tcs.SetException(e);
                }
            };

            return tcs.Task;
        }
    }
}
