﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Handlers.Selection;
using ProjectStructure.ApplicationServices.MapperProfiles;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;
using ProjectStructure.Tests.Abstractions.MockServices;
using Xunit;

namespace ProjectStructure.ApplicationServices.Tests.Queries
{
    public class SelectionQueries
    {
        private readonly IMapper _mapper;

        public SelectionQueries()
        {
            var mapperConfig = new MapperConfiguration(expression =>
            {
                expression.AddProfile(new UsersProfile());
                expression.AddProfile(new ProjectsProfile());
                expression.AddProfile(new TeamsProfile());
                expression.AddProfile(new TasksProfile());
            });

            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public async Task GetProjectSummariesQuery_SelectsAllProjects()
        {
            List<Project> projects = new()
            {
                new Project {Id = 1, Name = "First"},
                new Project {Id = 2, Name = "Second"},
                new Project {Id = 3, Name = "Not First"},
                new Project {Id = 43, Name = "43"}
            };

            IReadRepository<Project> repository = new MockReadOnlyRepository<Project>(projects);

            var request = new GetProjectSummariesQuery();
            var handler = new GetProjectSummariesHandler(repository, _mapper);

            var projectDTOs = _mapper.Map<List<ProjectReadDTO>>(projects);
            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Equal(result.Select(summary => summary.Project).ToHashSet(), projectDTOs.ToHashSet());
        }

        [Fact]
        public async Task GetSortedUsersQuery()
        {
            List<User> users = new()
            {
                new User {Id = 1, FirstName = "First"},
                new User {Id = 2, FirstName = "Second"},
                new User {Id = 3, FirstName = "Not First"},
                new User {Id = 43, FirstName = "43"}
            };

            IReadRepository<User> repository = new MockReadOnlyRepository<User>(users);

            var request = new GetSortedUsersQuery();
            var handler = new GetSortedUsersHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            var expectedResult = users.OrderBy(user => user.FirstName).Select(user => _mapper.Map<UserReadDTO>(user));

            Assert.Equal(result, expectedResult);
        }

        [Fact]
        public async Task GetTeamsWithOldEnoughUsers()
        {
            var now = new DateTimeOffset(2000, 12, 1, 12, 0, 0, TimeSpan.Zero);

            var oldEnoughDate = now.AddYears(-25);
            var youngDate = now.AddYears(-5);

            List<Team> teams = new()
            {
                new Team
                {
                    Id = 1,
                    Name = "Invalid",
                    Users = new List<User>
                    {
                        new User {BirthDay = oldEnoughDate},
                        new User {BirthDay = youngDate},
                        new User {BirthDay = oldEnoughDate}
                    }
                },
                new Team
                {
                    Id = 2,
                    Name = "Invalid",
                    Users = new List<User>
                    {
                        new User {BirthDay = youngDate}
                    }
                },
                new Team
                {
                    Id = 3,
                    Name = "Valid",
                    Users = new List<User>
                    {
                        new User {BirthDay = oldEnoughDate},
                    }
                },
            };

            IDateProvider dateProvider = new MockDateProvider(now);

            IReadRepository<Team> repository = new MockReadOnlyRepository<Team>(teams);

            var request = new GetTeamsWithOldEnoughUsersQuery();
            var handler = new GetTeamsWithOldEnoughUsersHandler(repository, dateProvider, _mapper);

            var result = (await handler.Handle(request, CancellationToken.None)).ToList();

            Assert.Equal(new []{2, 0, 1}, result.Select(x => x.Item3.Count()));
        }

        [Fact]
        public async Task GetUserSummaryQuery_WhenIdIsIncorrect()
        {
            List<User> users = new()
            {
                new User {Id = 1}
            };

            IReadRepository<User> repository = new MockReadOnlyRepository<User>(users);

            var request = new GetUserSummaryQuery(2);
            var handler = new GetUserSummaryHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Equal(result, new NotFound());
        }

        [Fact]
        public async Task GetUserSummaryQuery_WhenIdIsCorrect()
        {
            var startDate = new DateTimeOffset(2000, 12, 1, 12, 0, 0, TimeSpan.Zero);

            List<User> users = new()
            {
                new User
                {
                    Id = 1,
                    Tasks = new List<UserTask>()
                    {
                        new()
                        {
                            Id = 1,
                            CreatedAt = startDate,
                            FinishedAt = startDate.AddYears(10)
                        },
                        new()
                        {
                            Id = 2,
                            CreatedAt = startDate,
                            FinishedAt = startDate.AddYears(4)
                        },
                        new()
                        {
                            Id = 3,
                            CreatedAt = startDate,
                            FinishedAt = startDate.AddYears(12)
                        },
                        new()
                        {
                            Id = 4,
                            CreatedAt = startDate
                        }
                    }
                }
            };

            IReadRepository<User> repository = new MockReadOnlyRepository<User>(users);

            var request = new GetUserSummaryQuery(1);
            var handler = new GetUserSummaryHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.True(result.IsT0);

            var summary = result.AsT0;

            Assert.Equal(3, summary.LongestFinishedTask?.Id);
            Assert.Null(summary.LastProject);
            Assert.Equal(1, summary.UnfinishedTasksCount);
        }

        [Fact]
        public async Task GetUserTaskCountQuery_WhenIdIsIncorrect()
        {
            List<Project> projects = new()
            {
                new Project(),
                new Project
                {
                    Id = 1,
                    AuthorId = 1,
                    Tasks = Enumerable.Repeat(new UserTask {PerformerId = 1}, 5).ToList()
                }
            };

            IReadRepository<Project> repository = new MockReadOnlyRepository<Project>(projects);

            var request = new GetUserTaskCountQuery(2);
            var handler = new GetUserTaskCountHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetUserTaskCountQuery_WhenIdIsCorrect()
        {
            List<Project> projects = new()
            {
                new Project(),
                new Project
                {
                    Id = 1,
                    AuthorId = 1,
                    Tasks = Enumerable.Repeat(new UserTask {PerformerId = 1}, 5).ToList()
                }
            };

            IReadRepository<Project> repository = new MockReadOnlyRepository<Project>(projects);

            var request = new GetUserTaskCountQuery(1);
            var handler = new GetUserTaskCountHandler(repository, _mapper);

            var result = (await handler.Handle(request, CancellationToken.None)).ToList();

            Assert.Single(result);

            result.First().Deconstruct(out var projectDto, out var tasksCount);

            Assert.Equal(1, projectDto.Id);
            Assert.Equal(5, tasksCount);
        }

        [Fact]
        public async Task GetUserTasksFinishedInCurrentYearQuery_WhenIdIsIncorrect()
        {
            var currentYearDate = new DateTimeOffset().AddYears(2000);
            var notCurrentYearDate = currentYearDate.AddYears(-5);

            List<UserTask> tasks = new()
            {
                new() {Id = 1, PerformerId = 1, FinishedAt = currentYearDate},
                new() {Id = 2, PerformerId = 1, FinishedAt = currentYearDate},
                new() {Id = 3, PerformerId = 1, FinishedAt = notCurrentYearDate},
                new() {Id = 4, PerformerId = 1, FinishedAt = notCurrentYearDate},
                new() {Id = 5, PerformerId = 1, FinishedAt = currentYearDate},
            };

            IReadRepository<UserTask> repository = new MockReadOnlyRepository<UserTask>(tasks);

            IDateProvider dateProvider = new MockDateProvider(currentYearDate);

            var request = new GetUserTasksFinishedInCurrentYearQuery(2);
            var handler = new GetUserTasksFinishedInCurrentYearHandler(repository, dateProvider);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetUserTasksFinishedInCurrentYearQuery_WhenIdIsCorrect()
        {
            var currentYearDate = new DateTimeOffset().AddYears(2000);
            var notCurrentYearDate = currentYearDate.AddYears(-5);

            List<UserTask> tasks = new()
            {
                new() {Id = 1, PerformerId = 1, FinishedAt = currentYearDate},
                new() {Id = 2, PerformerId = 1, FinishedAt = currentYearDate},
                new() {Id = 3, PerformerId = 1, FinishedAt = notCurrentYearDate},
                new() {Id = 4, PerformerId = 1, FinishedAt = notCurrentYearDate},
                new() {Id = 5, PerformerId = 1, FinishedAt = currentYearDate},
            };

            IReadRepository<UserTask> repository = new MockReadOnlyRepository<UserTask>(tasks);

            IDateProvider dateProvider = new MockDateProvider(currentYearDate);

            var request = new GetUserTasksFinishedInCurrentYearQuery(1);
            var handler = new GetUserTasksFinishedInCurrentYearHandler(repository, dateProvider);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Equal(new List<int> {1, 2, 5}, result.Select(x => x.Item1));
        }

        [Fact]
        public async Task GetUserTaskQuery_WhenIdIsIncorrect()
        {
            List<UserTask> tasks = new()
            {
                new() {PerformerId = 1}
            };

            IReadRepository<UserTask> repository = new MockReadOnlyRepository<UserTask>(tasks);

            var request = new GetUserTasksQuery(2);
            var handler = new GetUserTasksHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            Assert.Empty(result);
        }

        [Fact]
        public async Task GetUserTaskQuery_WhenIdIsCorrect()
        {
            var longString = new string('a', 100);

            List<UserTask> tasks = new()
            {
                new() {PerformerId = 1, Name = "1"},
                new() {PerformerId = 1, Name = longString},
                new() {PerformerId = 1, Name = "1"},
                new() {PerformerId = 2, Name = "1"},
            };

            IReadRepository<UserTask> repository = new MockReadOnlyRepository<UserTask>(tasks);

            var request = new GetUserTasksQuery(1);
            var handler = new GetUserTasksHandler(repository, _mapper);

            var result = (await handler.Handle(request, CancellationToken.None)).ToList();

            Assert.Equal(2, result.Count);
        }

        [Fact]
        public async Task GetUserUnfinishedTaskQuery_WhenIdIsIncorrect()
        {
            List<User> users = new()
            {
                new() {Id = 1}
            };

            IReadRepository<User> repository = new MockReadOnlyRepository<User>(users);

            var request = new GetUserUnfinishedTasksQuery(2);
            var handler = new GetUserUnfinishedTasksHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            result.Switch(
                tasks => Assert.True(false),
                notFound => Assert.True(true)
            );
        }

        [Fact]
        public async Task GetUserUnfinishedTaskQuery_WhenIdIsCorrect()
        {
            List<User> users = new()
            {
                new()
                {
                    Id = 1,
                    Tasks = new List<UserTask>
                    {
                        new() {Id = 1},
                        new() {Id = 2, FinishedAt = new DateTimeOffset()},
                        new() {Id = 3}
                    }
                }
            };

            IReadRepository<User> repository = new MockReadOnlyRepository<User>(users);

            var request = new GetUserUnfinishedTasksQuery(1);
            var handler = new GetUserUnfinishedTasksHandler(repository, _mapper);

            var result = await handler.Handle(request, CancellationToken.None);

            result.Switch(
                tasks => Assert.Equal(2, tasks.Count()),
                notFound => Assert.True(false)
            );
        }
    }
}
