﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ProjectStructure.ApplicationServices.Commands.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Handlers.Tasks;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;
using ProjectStructure.Tests.Abstractions.MockServices;
using Xunit;

namespace ProjectStructure.ApplicationServices.Tests.Commands
{
    public class TasksCommands
    {
        [Fact]
        public async Task TaskSetFinished_WithValidId()
        {
            List<UserTask> tasks = new()
            {
                new UserTask {Id = 1},
                new UserTask {Id = 2},
            };

            IRepository<UserTask> repository = new MockRepository<UserTask>(tasks);

            IMapper mapper = new MapperConfiguration(x => x.CreateMap<TaskWriteDTO, UserTask>().ReverseMap())
                .CreateMapper();

            var taskDto = mapper.Map<TaskWriteDTO>(new UserTask
            {
                FinishedAt = new DateTimeOffset().AddYears(1000)
            });

            var request = new UpdateTaskCommand(1, taskDto);
            var handler = new UpdateTaskHandler(repository, mapper);

            await handler.Handle(request, CancellationToken.None);

            var task = await repository.GetAsync(1);

            Assert.NotNull(task?.FinishedAt);
        }

        [Fact]
        public async Task TaskSetFinished_WithInvalidId()
        {
            List<UserTask> tasks = new()
            {
                new UserTask {Id = 1},
                new UserTask {Id = 2},
            };

            IRepository<UserTask> repository = new MockRepository<UserTask>(tasks);

            IMapper mapper = new MapperConfiguration(x => x.CreateMap<TaskWriteDTO, UserTask>().ReverseMap())
                .CreateMapper();

            var taskDto = mapper.Map<TaskWriteDTO>(new UserTask
            {
                FinishedAt = new DateTimeOffset().AddYears(1000)
            });

            var request = new UpdateTaskCommand(0, taskDto);
            var handler = new UpdateTaskHandler(repository, mapper);

            await handler.Handle(request, CancellationToken.None);

            var task = await repository.GetAsync(1);

            Assert.Null(task?.FinishedAt);
        }
    }
}
