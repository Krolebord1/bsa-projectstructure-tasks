﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using FakeItEasy;
using ProjectStructure.ApplicationServices.Commands.Users;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Handlers.Users;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;
using ProjectStructure.Tests.Abstractions.MockServices;
using Xunit;

namespace ProjectStructure.ApplicationServices.Tests.Commands
{
    public class UsersCommands
    {
        [Fact]
        public async Task CreateUser()
        {
            var repository = A.Fake<IRepository<User>>();

            var mapper = A.Fake<IMapper>();

            var dateProvider = A.Fake<IDateProvider>();

            var request = new CreateUserCommand(A.Fake<UserWriteDTO>());
            var handler = new CreateUserHandler(repository, mapper, dateProvider);

            await handler.Handle(request, CancellationToken.None);

            A.CallTo(() => repository.SaveChangesAsync()).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task AddUserToTeam()
        {
            List<User> users = new()
            {
                new User {Id = 1, TeamId = 1},
                new User {Id = 2, TeamId = null},
            };

            IRepository<User> repository = new MockRepository<User>(users);

            IMapper mapper = new MapperConfiguration(x => x.CreateMap<UserWriteDTO, User>().ReverseMap())
                .CreateMapper();

            var userDto = mapper.Map<UserWriteDTO>(new User
            {
                TeamId = 2
            });

            var request = new UpdateUserCommand(2, userDto);
            var handler = new UpdateUserHandler(repository, mapper);

            await handler.Handle(request, CancellationToken.None);

            var user = await repository.GetAsync(2);

            Assert.Equal(2, user?.TeamId);
        }
    }
}
